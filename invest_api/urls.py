from django.urls import path
from invest_api.views import AtivosView, TransacoesView


urlpatterns = [
    path('ativos/', AtivosView.as_view()),
    path('transacoes/', TransacoesView.as_view()),
]
