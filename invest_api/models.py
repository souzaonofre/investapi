
from django.db import models
from django.contrib.auth.models import User

MODALIDADE_CHOICES_TYPES = [
    'renda_fixa',
    'renda_variavel',
    'cripto'
]

MODALIDADE_CHOICES_MAP = [
    ('renda_fixa', 'Renda Fixa'),
    ('renda_variavel', 'Renda Variável'),
    ('cripto', 'Criptomoeda')
]

OPERACAO_CHOICES_TYPES = [
    'aplicacao',
    'resgate'
]

OPERACAO_CHOICES_MAP = [
    ('aplicacao', 'Aplicação'),
    ('resgate', 'Regaste')
]


class Ativo(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.PROTECT)
    nome = models.CharField(max_length=200, unique=True)
    modalidade = models.CharField(
        max_length=40,
        choices=MODALIDADE_CHOICES_MAP,
        default=MODALIDADE_CHOICES_TYPES[0]
    )

    def __str__(self):
        return self.nome


class Transacao(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.PROTECT)
    ativo = models.ForeignKey(Ativo, on_delete=models.PROTECT)
    data_solicitacao = models.DateField(auto_now=True)
    operacao = models.CharField(
        max_length=10,
        choices=OPERACAO_CHOICES_MAP,
        default=OPERACAO_CHOICES_TYPES[0]
    )
    quantidade = models.IntegerField(default=1)
    preco_unitario = models.DecimalField(max_digits=100, decimal_places=2)

    def __str__(self):
        return f"{self.data_solicitacao}:{self.ativo}"
