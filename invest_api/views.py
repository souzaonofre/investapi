from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions

from invest_api.models import Ativo
from invest_api.serializers import AtivoSerializer, TransacaoSerializer


class AtivosView(APIView):
    """
    Ativos APIView endpoints.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, format='json'):
        """
        Return a list of all ativos.
        """
        serializer = AtivoSerializer(Ativo.objects.all())
        return Response(serializer.data)

    def post(self, request, format='json'):
        """
        Create new ativo data.
        """
        serializer = AtivoSerializer(data=request.data, request=request)

        if serializer.is_valid():
            serializer.create()
            return Response(serializer.data)
        else:
            return Response()


class TransacoesView(APIView):
    """
    Transacoes APIView endpoints.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, format='json'):
        """
        Return a list of all transacoes.
        """
        serializer = TransacaoSerializer()
        return Response(serializer.data)

    def post(self, request, format='json'):
        """
        Create new ativo data.
        """
        serializer = TransacaoSerializer(data=request.data, request=request)

        if serializer.is_valid():
            serializer.create()
            return Response(serializer.data)
        else:
            return Response()
