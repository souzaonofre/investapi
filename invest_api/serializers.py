from rest_framework import serializers
from invest_api.models import Ativo, Transacao, MODALIDADE_CHOICES_TYPES
from datetime import datetime


class AtivoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ativo
        fields = ['id', 'nome', 'modalidade']

    def validate_modalidade(self, value):
        if value in MODALIDADE_CHOICES_TYPES:
            return value
        else:
            raise serializers.ValidationError("'modalidade' invalid data")

    def create(self, validated_data, request):
        ativo = Ativo.objects.create(
            usuario=request.user,
            nome=validated_data['nome'],
            modalidade=validated_data['modalidade']
        )
        return ativo


class TransacaoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transacao
        fields = ['data_solicitacao', 'ativo', 'quantidade', 'preco_unitario']

    def create(self, validated_data, request):
        transacao = Transacao.objects.create(
            data_solicitacao=datetime.now(),
            usuario=request.user,
            ativo=validated_data['ativo'],
            operacao=validated_data['operacao'],
            quantidade=validated_data['quantidade'],
            preco_unitario=validated_data['preco_unitario']
        )
        return transacao
