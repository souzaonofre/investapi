from django.contrib import admin
from .models import Ativo, Transacao

admin.site.register(Ativo)
admin.site.register(Transacao)
