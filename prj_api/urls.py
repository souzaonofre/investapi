"""api URL Configuration
"""
from django.contrib import admin
from rest_framework.schemas import get_schema_view
from django.urls import path, include


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/invest/', include('invest_api.urls')),
    path('api/v1/', include('djoser.urls')),
    path('api/v1/', include('djoser.urls.authtoken')),

    path('openapi/', get_schema_view(
            title="Invest API",
            description="API for store Invest Assets transactions",
            version="0.0.1",
            urlconf='invest_api.urls'
        ), name='openapi-schema'),
]
